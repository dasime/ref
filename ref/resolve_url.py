import re
from urllib import error as urllib_error
from urllib import request


class TitleResolveError(Exception):
    def __init__(self, url: str, message: str = "Error during title lookup"):
        self.url = url
        self.message = message
        super().__init__(self.message)


def resolve_url_title(url: str) -> str:
    try:
        with request.urlopen(url) as res:
            content_type = res.headers.get("Content-Type")

            if content_type and "html" not in content_type:
                raise TitleResolveError(
                    url,
                    f"Problem detecting page title. Target doesn't appear to be HTML ({content_type})",
                )

            if res.status == 200:
                body = res.read()

                # Many websites fail to report the character encoding of the
                # webpage in the headers. Although the specification says
                # to default to `ISO-8859-1`, experience has shown many sites
                # will not advertise character encoding and use `UTF-8`.
                #
                # However, many pages follow the specification to default to
                # `ISO-8859-1`, so try that if `UTF-8` decoding fails.
                #
                # I'm yet to come across any other website character encodings
                # in the wild, although I'm certain they exist.
                #
                # Supported Python decoding:
                # https://docs.python.org/3/library/codecs.html#standard-encodings
                try:
                    decoded_body = body.decode("utf_8")
                except UnicodeDecodeError:
                    decoded_body = body.decode("latin_1")

                """
                <title>How to Pose for a Photograph - The New York Times</title>
                <title data-rh="true">How to Pose for a Photograph - The New York Times</title>
                """
                re_match = re.search(
                    r"<title.*>\s*(.*)\s*</title>", decoded_body
                )
                if re_match:
                    return re.sub(r"[\t\n\r\f\v]*", "", re_match.group(1))

            raise TitleResolveError(url, "Problem detecting page title")
    except urllib_error.HTTPError:
        # These are almost always 404 errors from blacklisted sites
        raise TitleResolveError(url, "Problem looking up page title")
