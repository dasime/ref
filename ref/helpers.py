import re
from html import unescape
from typing import Literal

from unihandecode import Unihandecoder  # type: ignore

SUPPORTED_CJK_HINTS = Literal["ja", "zh", "kr", "vn"]


def generate_slug(title: str, cjk_hint: SUPPORTED_CJK_HINTS = "ja") -> str:
    decode_cjk_hint = Unihandecoder(lang=cjk_hint)

    # Unescape HTML (&amp; -> &)
    slug = unescape(title)
    # Romanise CJK (with a preference for JA over ZH), convert to lowercase
    slug = decode_cjk_hint.decode(slug).lower()
    # Drop non-word and non-whitespace
    slug = re.sub(r"[^\w\s-]", "", slug)
    # Replace one-or-more "-" and spaces with "-"
    slug = re.sub(r"[-\s]+", "-", slug)
    # Strip leading "-", truncate to first 73 characters, strip trailing "-"
    slug = slug.lstrip("-")[:73].rstrip("-")

    return slug
