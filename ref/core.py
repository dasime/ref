from datetime import date, datetime
from typing import Any, Literal, Optional

from beartype.door import is_bearable
from typing_extensions import Self


class Ref:
    __slots__ = [
        "title",
        "url",
        "date",
        "access_date",
        "keywords",
        "visibility",
        "archive_url",
        "archive_date",
        "url_status",
        "quote",
        "notes",
        "language",
    ]

    title: Optional[str]
    url: Optional[str]
    date: Optional[date]
    access_date: Optional[datetime]
    keywords: Optional[list[str]]
    visibility: Optional[Literal["public", "private"]]
    archive_url: Optional[str]
    archive_date: Optional[datetime]
    url_status: Optional[Literal["live", "dead", "usurped"]]
    quote: Optional[str]
    notes: Optional[str]
    language: Optional[str]

    def __init__(self, *args: list[None], **kwargs: Any) -> None:
        if args:
            # kwargs only, too many options to make sense supporting positional
            raise TypeError("Positional arguments are not supported")

        for key, value in kwargs.items():
            setattr(self, key, value)

    def __setattr__(self, name: str, value: Any) -> None:
        if name not in self.__slots__:
            raise AttributeError(f"Invalid keys in data '{name}'")

        key_type = self.__annotations__[name]
        if not is_bearable(value, key_type):
            raise TypeError(f"'{name}' must be a '{key_type}'")

        super().__setattr__(name, value)

    # Helpers for underscore/hyphen conventions

    @staticmethod
    def _hyphen_to_underscore(data: dict[str, Any]) -> dict[str, Any]:
        return {key.replace("-", "_"): value for key, value in data.items()}

    @staticmethod
    def _underscore_to_hyphen(data: dict[str, Any]) -> dict[str, Any]:
        return {key.replace("_", "-"): value for key, value in data.items()}

    # Utilities for converting to and from

    @classmethod
    def from_dict(cls, data: dict[str, Any]) -> Self:
        normalised_data = cls._hyphen_to_underscore(data)
        if any(key not in cls.__slots__ for key in normalised_data):
            raise ValueError("Invalid keys in data")
        return cls(**normalised_data)

    def to_dict(self) -> dict[str, Any]:
        data = {
            key: getattr(self, key, None)
            for key in self.__slots__
            if getattr(self, key, None) is not None
        }
        return self._underscore_to_hyphen(data)
