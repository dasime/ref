from typing import get_args

import click

from ref.helpers import SUPPORTED_CJK_HINTS, generate_slug


@click.command()
@click.argument(
    "title",
    type=str,
    required=True,
)
@click.option(
    "--lang",
    "lang",
    default="ja",
    type=click.Choice(get_args(SUPPORTED_CJK_HINTS)),
    required=False,
    help="CJK language hint",
)
@click.option(
    "-n",
    "no_newline",
    default=False,
    is_flag=True,
    required=False,
    help="Do not output the trailing newline",
)
def slug(title: str, lang: SUPPORTED_CJK_HINTS, no_newline: bool) -> None:
    """Generate a slug based on an existing title."""
    slug = generate_slug(title, lang)

    if no_newline:
        print(slug, end="")
    else:
        print(slug)
