import click

from ref.read import ResolveLinkError, open_link


@click.command(name="open")
@click.argument("filename", type=click.Path(exists=True))
@click.option(
    "--archive",
    "archive",
    default=False,
    is_flag=True,
    required=False,
    help="Force open the archive-url",
)
@click.option(
    "--browser/--no-browser",
    type=bool,
    default=True,
    help="Attempt to open in your default web browser",
)
def open_(filename: str, archive: bool, browser: bool) -> None:
    """Open the URL in a reference, fallback to Archive URL if dead or usurped."""
    try:
        open_link(filename, archive, browser)
    except ResolveLinkError as e:
        click.echo(e.message, err=True)
        exit(1)
