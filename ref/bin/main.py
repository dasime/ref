import importlib.metadata as importlib_metadata
import sys

import click

from .import_ import import_
from .new import new
from .open_ import open_
from .slug import slug

sys.stdout.reconfigure(encoding="utf-8")  # type: ignore
sys.stderr.reconfigure(encoding="utf-8")  # type: ignore

__version__ = importlib_metadata.version("ref")


@click.group()
@click.version_option(__version__)
def cli() -> None:
    """Ref management."""
    pass


cli.add_command(import_)
cli.add_command(new)
cli.add_command(open_)
cli.add_command(slug)
