from datetime import datetime

import click

from ref.file.ref import write as write_ref
from ref.new import create_new_ref


# TODO: `--output` (either CWD or special central location)
# TODO: `--interactive` (guided creation or full-auto)
@click.command()
@click.option(
    "--access_date",
    "access_date",
    default=datetime.now().astimezone().replace(microsecond=0).isoformat(),
    type=click.DateTime(
        [
            "%Y-%m-%dT%H:%M:%S.%f%z",  # ISO-8601 with MS
            "%Y-%m-%dT%H:%M:%S%z",  # ISO-8601 without MS
            "%Y-%m-%dT%H:%M:%SZ",  # ISO-8601 without MS in UTC
            "%Y-%m-%dT%H:%M:%S.%f",  # ISO-8601 with MS without TZ
            "%Y-%m-%dT%H:%M:%S",  # ISO-8601 without MS without TZ
            "%Y-%m-%d %H:%M:%S",  # without 'T', without MS, without TZ
            "%Y-%m-%d %H:%M",  # without 'T', without S, without MS, without TZ
            "%d %B %Y, %H:%M:%S",  # GB format: "01 January 2001, 01:01:01"
            "%Y-%m-%d",  # YMD
        ]
    ),
    required=False,
    help="access-date of the ref",
)
@click.option(
    "--url",
    "url",
    type=str,
    required=False,
    help="URL of the ref",
)
@click.option(
    "--title",
    "title",
    type=str,
    required=False,
    help="Title of the ref",
)
@click.option(
    "--archive/--no-archive",
    type=bool,
    default=True,
    help="Attempt to find in the Internet Archive",
)
def new(access_date: datetime, url: str, title: str, archive: bool) -> None:
    """Create a new ref."""
    if not url:
        raise NotImplementedError("Non-URL refs are not currently supported")

    # If the timezone was not specified, assume local timezone
    if (
        access_date.tzinfo is None
        or access_date.tzinfo.utcoffset(access_date) is None
    ):
        access_date = access_date.astimezone()

    ref = create_new_ref(access_date, url, title, archive)

    write_ref(ref)
