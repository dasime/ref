import click

from ref.file.favouritier import read as read_fyp
from ref.file.ref import write as write_ref
from ref.file.windows_shortcut import read as read_windows_shortcut
from ref.helpers import generate_slug


@click.group(name="import")
def import_() -> None:
    """Import data from an existing format."""
    pass


# TODO: Set timezone to import from (mtime is always "local"
#       but it currently defaults to "Europe/London").
@import_.command()
@click.argument("filename", type=click.Path(exists=True))
@click.option(
    "--filename-as-title",
    "filename_as_title",
    default=False,
    is_flag=True,
    required=False,
    help="Use the filename as the title",
)
@click.option(
    "--archive/--no-archive",
    type=bool,
    default=True,
    help="Attempt to find in the Internet Archive",
)
def url(filename: str, filename_as_title: bool, archive: bool) -> None:
    """Import a Windows Shortcut (.URL file)."""
    ref = read_windows_shortcut(filename, filename_as_title, archive)

    write_ref(ref)


@import_.command()
@click.argument("filename", type=click.Path(exists=True))
def fyp(filename: str) -> None:
    """Import the fyp/favouritier format (.json file)."""
    for ref in read_fyp(filename):
        print(f"I: {generate_slug(ref.title or 'undefined')}")
        write_ref(ref)
