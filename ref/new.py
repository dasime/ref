from datetime import datetime
from html import unescape
from typing import Optional

from ref.core import Ref
from ref.resolve_internet_archive import ArchiveError, resolve_internet_archive
from ref.resolve_url import TitleResolveError, resolve_url_title


def create_new_ref(
    access_date: datetime,
    url: str,
    manual_title: Optional[str] = None,
    archive: Optional[bool] = True,
) -> Ref:
    ref = Ref()

    if manual_title:
        title = manual_title
    else:
        try:
            title = resolve_url_title(url)
        except TitleResolveError as e:
            # Best effort, no worries!
            # Lots of sites block crawlers, I don't want to fight them
            # Many sites use fancy certs not natively supported, that's fine
            print(f"{e.message}: {e.url}")
            title = url

    ref.title = unescape(title)

    ref.url = url
    ref.access_date = access_date
    ref.visibility = "public"

    if archive:
        try:
            archive_url, archive_date = resolve_internet_archive(
                url, access_date
            )

            ref.archive_url = archive_url
            ref.archive_date = archive_date
        except ArchiveError as e:
            # Archiving is best effort, if it fails there is nothing we can do
            print(f"{e.message}: {e.url}")
            pass

    # Default to assuming the original link still works
    ref.url_status = "live"

    return ref
