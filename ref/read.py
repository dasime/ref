import os
import webbrowser

from ref.core import Ref
from ref.file.ref import read as read_ref
from ref.file.windows_shortcut import read as read_windows_shortcut


class ResolveLinkError(Exception):
    def __init__(self, message: str = "Error during link resolution"):
        self.message = message
        super().__init__(self.message)


def _resolve_file(filename: str, force_archive: bool) -> Ref:
    _, ext = os.path.splitext(filename)

    ext = ext.lower()

    if ext == ".toml":
        # Looks like "ref"
        return read_ref(filename)
    elif ext == ".url":
        # Looks like "Windows Shortcut"
        return read_windows_shortcut(filename, True, force_archive)
    else:
        raise ValueError(f"Unsupported filetype detected: '{ext}'")


def _resolve_link(ref: Ref, force_archive: bool = False) -> str:
    url_attempt_failed = False

    url = ref.url if hasattr(ref, "url") else None
    url_status = ref.url_status if hasattr(ref, "url_status") else None
    archive_url = ref.archive_url if hasattr(ref, "archive_url") else None

    if not force_archive:
        if url:
            if (url_status is None) or (url_status == "live"):
                return url
            else:
                url_attempt_failed = True

    if archive_url:
        return archive_url
    else:
        if url_attempt_failed:
            raise ResolveLinkError(
                f"Link is marked {url_status} and no archive found"
            )
        if force_archive:
            raise ResolveLinkError("No archive link found")

    raise ResolveLinkError("No link found to open :(")


def open_link(
    filename: str, force_archive: bool = False, browser: bool = True
) -> None:
    ref = _resolve_file(filename, force_archive)
    url = _resolve_link(ref, force_archive)

    if browser:
        print(f"Opening {url}")
        webbrowser.open(url)
    else:
        print(url)

    return
