import re
from datetime import datetime, timezone
from typing import Tuple
from urllib import error as urllib_error
from urllib import request


class ArchiveError(Exception):
    def __init__(self, url: str, message: str = "Error during archive lookup"):
        self.url = url
        self.message = message
        super().__init__(self.message)


def resolve_internet_archive(
    original_url: str, access_date: datetime
) -> Tuple[str, datetime]:
    """
    url = "http://www.saveie6.com/"
    access-date = 2010-02-15T18:22:44Z

    $ curl https://web.archive.org/web/20100215182244/http://www.saveie6.com/ -v
    ...
    > GET /web/20100215182244/http://www.saveie6.com/ HTTP/2
    > Host: web.archive.org
    > user-agent: curl/7.68.0
    > accept: */*
    ...
    < x-archive-redirect-reason: found capture at 20100214030930
    < location: https://web.archive.org/web/20100214030930/http://www.saveie6.com/
    """
    # web.archive.org stores the date of the capture in the URL as /YYYYmmddHHMMSS/.
    INTERNET_ARCHIVE_DATETIME_FORMAT = "%Y%m%d%H%M%S"
    # TODO: Resolve datetime tz offset to UTC - the current version ignores TZ
    formatted_datetime = datetime.strftime(
        access_date, INTERNET_ARCHIVE_DATETIME_FORMAT
    )
    archive_lookup_attempt = (
        f"https://web.archive.org/web/{formatted_datetime}/{original_url}"
    )

    try:
        with request.urlopen(archive_lookup_attempt) as archive_res:
            if archive_res.status == 200:
                # This date is supposed to always be UTC.
                # See: https://en.wikipedia.org/wiki/Help:Using_the_Wayback_Machine#Specific_archive_copy
                archive_date_match = re.search(
                    r"https:\/\/web.archive.org\/web\/(\d{14})\/.*",
                    archive_res.url,
                )
                if archive_date_match:
                    archive_date_raw = archive_date_match.group(1)
                else:
                    raise ArchiveError(
                        archive_res.url, "Error processing IA URL"
                    )

                archive_date_native = datetime.strptime(
                    archive_date_raw, INTERNET_ARCHIVE_DATETIME_FORMAT
                )
                archive_date = archive_date_native.replace(tzinfo=timezone.utc)

                return (
                    archive_res.url,
                    archive_date,
                )
            else:
                raise ArchiveError(
                    archive_lookup_attempt, "Unexpected response from IA"
                )
    except urllib_error.HTTPError:
        # These are almost always 404 errors from blacklisted sites
        raise ArchiveError(archive_lookup_attempt)
    except urllib_error.URLError as err:
        # These are almost always timeout errors from when IA is under load
        raise ArchiveError(
            archive_lookup_attempt, f"Problem connecting to IA: '{err.reason}'"
        )
