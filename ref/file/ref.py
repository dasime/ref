import os

from tomlkit import dumps, parse

from ref.core import Ref
from ref.helpers import generate_slug


def read(filename: str) -> Ref:
    with open(filename, "r") as ref_buf:
        ref_toml = parse(ref_buf.read())
        ref = Ref.from_dict(ref_toml.unwrap())

        return ref


# TODO: Allow output_directory rather than just CWD
def write(ref: Ref) -> None:
    if not isinstance(ref.title, str) or len(ref.title) == 0:
        print("Write aborted, ref title not detected")
        exit(1)

    ref_slug = generate_slug(ref.title)
    name = f"{ref_slug}.toml"

    if os.path.exists(name):
        print(f"Write aborted, file already exists at: {name}")
        exit(1)

    out_file = os.path.abspath(name)

    with open(out_file, mode="w", encoding="utf-8", newline="\n") as f:
        f.write(dumps(ref.to_dict()))
