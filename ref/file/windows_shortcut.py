import os
import pathlib
import re
from datetime import datetime

import pytz

from ref.core import Ref
from ref.new import create_new_ref


def read(
    filename: str,
    filename_as_title: bool,
    archive: bool,
    system_timezone: str = "Europe/London",
) -> Ref:
    """Ingest Windows Shortcuts (.URL)."""
    local_timezone = pytz.timezone(system_timezone)

    modified_time_timestamp = os.path.getmtime(filename)
    modified_time_local = datetime.fromtimestamp(modified_time_timestamp)
    modified_time_offset = modified_time_local.astimezone(local_timezone)

    with open(filename, "r") as link_buf:
        link_str = link_buf.read()
        re_match = re.search(r"URL=(.*)", link_str)
        if re_match:
            url = re_match.group(1)
        else:
            print(f"No URL address found in {filename}")
            raise

    manual_title = None

    if filename_as_title:
        shortcut_filepath = pathlib.Path(filename)
        manual_title = shortcut_filepath.stem

    return create_new_ref(modified_time_offset, url, manual_title, archive)
