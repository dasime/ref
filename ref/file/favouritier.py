import json
from datetime import datetime
from html import unescape
from typing import Generator

from ref.core import Ref
from ref.resolve_internet_archive import ArchiveError, resolve_internet_archive


def read(favouriter_db: str) -> Generator[Ref, None, None]:
    with open(
        favouriter_db,
        mode="r",
        encoding="utf-8",
    ) as favouriter_db_raw:
        favouriter_db_parsed = json.load(favouriter_db_raw)
        for bookmark in favouriter_db_parsed["favourites"]:
            ref = Ref()

            ref.title = unescape(bookmark["title"])
            ref.url = bookmark["url"]
            parsed_access_date = datetime.fromisoformat(bookmark["date"])
            ref.access_date = parsed_access_date
            ref.keywords = bookmark["keywords"]
            if bookmark.get("notes"):
                ref.notes = unescape(bookmark["notes"])
            ref.visibility = "private" if bookmark["status"] == 0 else "public"

            try:
                archive_url, archive_date = resolve_internet_archive(
                    bookmark["url"], parsed_access_date
                )

                ref.archive_url = archive_url
                ref.archive_date = archive_date
            except ArchiveError as e:
                # Archiving is best effort, if it fails there is nothing we can do
                print(f"{e.message}: {e.url}")
                pass

            # Default to assuming the original link still works
            ref.url_status = "live"

            yield ref
