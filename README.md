# Ref

A terse-ish system for recording references.
This is a total rewrite of the original `Favouritier`.

## Interface

Call `ref --help` for more details:

```shell
$ ref --help
Usage: ref [OPTIONS] COMMAND [ARGS]...

  Ref management.

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  import  Import data from an existing format.
  new     Create a new ref.
  open    Open the URL in a reference, fallback to Archive URL if dead or...
  slug    Generate a slug based on an existing title.
```

You can call `--help` after any command for more documentation.

## Quick Start

To create a new ref for a website:

```shell
ref new --url https://example.com/
```

To open a ref with an address in it:

```shell
ref open example-domain.toml
```

To generate a slug for an existing ref (with CJK romanising):

```shell
$ ref slug "[ABC] Long Title 'with-punctuation' &   (other sad &nbsp; issues)"
abc-long-title-with-punctuation-other-sad-issues

$ ref slug 日本語 --lang ja
nihongo

$ ref slug 中文 --lang zh
zhong-wen
```

## The Ref Schema

Ref filenames MUST follow this pattern: `[a-z-]{0,73}.toml`

Ref filenames MUST only consist of lowercase A-Z and hyphens.
Unicode filenames are discouraged because of how inconsistently they are
supported across different filesystems, protocols and programs.

Ref filenames not including extension (aka _slug_) MUST be at most 73 characters.
This is to leave enough room for the `.toml` file extension (5 characters)
and EoL characters (`CRLF` if you're unlucky) on an 80 character prompt.

Internally, ref files use the [TOML format](https://toml.io/).
This is a text-based format, similar to `.ini` but with an actual specification
and consistent parser support.

As per the TOML spec, Ref files MUST be a valid UTF-8 encoded Unicode document.

Fields in the Ref schema are inspired partially by Wikipedia's Cite template:
<https://en.wikipedia.org/wiki/Template:Citation>

```toml
title = "String"
url = "String"
date = "Date-Time"
access-date = "Offset Date-Time"
keywords = ["String"]
visibility = "Literal['public', 'private']" # Default "public"
archive-url = "String"
archive-date = "Offset Date-Time"
url-status = "Literal['live', 'dead', 'usurped']" # Default "live"
quote = """String""" # Markdown support
notes = """String""" # Markdown support
language = "String" # ISO 639-1 (two letter) language code
```

Ref files MUST have an `access-date` field.
Although only setting `access-date` would be quite a boring reference by itself.

Ref files SHOULD have the `language` field set whenever a reference is
not in your default language.

All other documented fields are OPTIONAL and are only set when relevant.
